import pandas as pd
import numpy as np
import matplotlib.pyplot as mp
import os
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim as optim
from torch.autograd import Variable

class boltzmann_machine():
    '''
    Categoria: Não supervisionado
    Escopo: Sistema de Recomendação, exemplo: recomendação de filmes
    '''
    def __init__(self, users, movies, ratings, training_set, 
                 test_set, nb_users, nb_movies):

        import torch
        import torch.nn as nn
        import torch.nn.parallel
        import torch.optim as optim
        from torch.autograd import Variable

        self.users = users
        self.movies = movies
        self.ratings = ratings
        self.training_set = training_set
        self.test_set = test_set
        self.nb_users = nb_users
        self.nb_movies = nb_movies
        self.epoch = 10

        # Initialize the weights randomly 
        self.W = torch.randn(100, len(training_set[0]))

        # Initialize the bias - Probability of hidden nodes 
        self.a = torch.randn(1, 100)
        
         # Initialize the bias - Probability of visible nodes 
        self.b = torch.randn(1, len(training_set[0])
        
        None
    

    def convert(self, data):
        new_data = []
        
        for id_users in range(1, self.nb_users+1):
            id_movies = data[:, 1][data[:,0] == id_users]
            id_ratings = data[:, 2][data[:,0] == id_users]
            ratings = np.zeros(nb_movies)
            ratings[id_movies-1] = id_ratings
            new_data.append(list(ratings))
    

    def sample_h(self, x):
        '''
        x: The visible neurons with probability  ph given v
        '''
        # multiplying two torchs objects (self.W.t() taking the transpose)
        wx = torch.mm(x, self.W.t())

        # activate is the probability of the hidden node activate given the visible node value
        activation = wx + self.a.expand_as(wx)

        # p_h_given_v: probability of the hidden node activate given the visible node value
        p_h_given_v = torch.sigmoid(activation)

        # returning probability of they be activate and the respective sample 
        return p_h_given_v, torch.bernoulli(p_h_given_v)

    
    def sample_v(self, y):

        wy = torch.mm(y, self.W())
        activation = wy + self.b.expand_as(wy)
        p_v_given_h = torch.sigmoid(activation)
        return p_v_given_h, torch.bernoulli(p_v_given_h)


    def train(self, v0, vk, ph0, phk):

        self.W += torch.mm(v0.t(), ph0) - torch.mm(vk.t(), phk)
        self.b += torch.sum((v0-vk), 0)
        self.a += torch.sum((ph0 - phk), 0)


    def training_rbm(self):

        for epoch in range(1, self.nb_epoch + 1)
            train_loss = 0
            s = 0
            for id_user in range(0, nb_users - batch_size, batch_size)
                vk = training_set[id_user:id_user+batch_size]
                v0 = training_set[id_user:id_user+batch_size]
                ph0, _ = self.sample_h(vk)
                for k in range(10):
                    _, hk = rbm.sample_h(vk)
                    _, vk = rbm.sample_v(hk)
                    vk[v0<0] = v0[v0<0]
                phk, _ = self.sample_h(vk)
                self.train(v0, vk, ph0, phk)
                train_loss += torch.mean(torch.abs(v0[v0>=0] - vk[v0>=0]))
                s += 1
            print('epoch: '+str(epoch)+' loss: '+str(train_loss/s))


    def testing_rbm(self):

        test_loss = 0
        s = 0
        for id_user in range(self.nb_users):
            v = self.training_set[id_user:id_user+1]
            vt = self.test_set[id_user:id_user+1]
            if len(vt[vt>=0]) > 0:
                _, h = self.sample_h(v)
                _, v = self.sample_v(h)
                test_loss += torch.mean(torch.abs(vt[vt>=0] - v[vt>=0]))

        print('test loss: ' + str(test_loss/s))



if __name__ == '__main__':


    file_path = os.path.join(os.getcwd(), 'ml-1m')
    
    file_train_path = os.path.join(file_path, 'movies.dat')
    movies_path = pd.read_csv(file_train_path, 
                          sep='::', 
                          header=None, 
                          engine='python', 
                          encoding='latin-1')
    
    users_path = os.path.join(file_path, 'users.dat')
    users = pd.read_csv(file_train_path, 
                          sep='::', 
                          header=None, 
                          engine='python', 
                          encoding='latin-1')

    ratings_path = os.path.join(file_path, 'ratings.dat')
    ratings = pd.read_csv(ratings_path, 
                          sep='::', 
                          header=None, 
                          engine='python', 
                          encoding='latin-1')
    
    # Preparing the training set and the test set
    ratings_path = os.path.join(file_path, 'ratings.dat')
    ratings = pd.read_csv(ratings_path, 
                          sep='::', 
                          header=None, 
                          engine='python', 
                          encoding='latin-1')
    
    
    file_path = os.path.join(file_path, 'ml-100k')
    training_set_path = os.path.join(file_path, 'u1.base')
    
    training_set = pd.read_csv(training_set_path, 
                               delimiter='\t',
                               header=None)
    
    training_set = np.array(training_set, dtype='int')
    
    test_set_path = os.path.join(file_path, 'u1.test')
    
    test_set = pd.read_csv(training_set_path, 
                               delimiter='\t',
                               header=None)
    
    test_set = np.array(test_set, dtype='int')
    
    # Getting the number of users and movies
    nb_users = int(max(max(training_set[:, 0]), max(test_set[:,0])))
    nb_movies = int(max(max(training_set[:, 1]), max(test_set[:,1])))
    
    #bm = boltzmann_machine(users, ratings, training_set, 
    #                       test_set, nb_users, nb_movies)
    
    #bm.convert()
    
    def convert(data):
        new_data = []
        
        for id_users in range(1, nb_users+1):
            id_movies = data[:, 1][data[:,0] == id_users]
            id_ratings = data[:, 2][data[:,0] == id_users]
            ratings = np.zeros(nb_movies)
            ratings[id_movies-1] = id_ratings
            new_data.append(list(ratings))
            
        return new_data
    
    training_set = convert(training_set)
    test_set = convert(test_set)
    
    # Converting the data into Torch tensors
    training_set = torch.FloatTensor(training_set)
    test_set = torch.FloatTensor(test_set)
    
    # Converting the ratings into binary ratings 1 (Liked) or 0 (Not Liked)
    training_set[training_set == 0] = -1
    training_set[training_set == 1] = 0
    training_set[training_set == 2] = 0
    training_set[training_set >= 3] = 1
    test_set[test_set == 0] = -1
    test_set[test_set == 1] = 0
    test_set[test_set == 2] = 0
    test_set[test_set >= 3] = 1
